<?php

namespace Abonive\JWTAuth;

use \Firebase\JWT\JWT;
use \Firebase\JWT\ExpiredException as JwtExpiredException;
use \Firebase\JWT\SignatureInvalidException;

use Abonive\JWTAuth\Exceptions\ExpiredException;
use Abonive\JWTAuth\Exceptions\InvalidSignatureException;

class Auth
{	
	//Key
	private $key;

	//Token
	private $token;

	//Token Settings
	private $tokenId;
	private $issuedAt;
	private $notBefore;
	private $expire;
	private $data;

	private $uid;
	
	function __construct()
	{
		$this->key = "2uBO0A/OeOaqdfJolaTAlghuHpDPgf31pxdC0Ch0rF4lkyA+jbTubD6k4vkM6Gef";
		// $this->generateToken();
	}

	public function decodeToken($token)
	{
		try{
			$var = JWT::decode($token, $this->key, array('HS256'));
			return $var;
		}catch(JwtExpiredException $e){
			throw new ExpiredException('Expirado');
		}catch(SignatureInvalidException $e){
			throw new InvalidSignatureException('invalid');
		}
		
	}

	public function getToken($user_id)
	{
		$this->uid = $user_id;
		$this->generateToken();
		return $this->token;
	}

	private function generateToken(){
		$this->setTokenId();

     	$this->issuedAt   = time();
     	$this->notBefore  = $this->issuedAt;
     	$this->expire     = $this->notBefore + (7 * 24 * 60 * 60);

     	$this->data = array(
			'jti' => $this->tokenId,
		    "iss" => "http://reparo24h.es",
		    "aud" => "http://reparo24h.es",
		    "iat" => $this->issuedAt,
		    "nbf" => $this->notBefore,
		    "exp" => $this->expire,
		    "uid" => $this->uid,
		);
		$this->token = JWT::encode($this->data, $this->key);
	}

	private function setTokenId(){
		$this->tokenId = base64_encode(mcrypt_create_iv(20));
	}
}
?>
