<?php

namespace Abonive\JWTAuth;

use Closure;

class ApiMiddleware
{
    public function handle($request, Closure $next)
    {

		if(strpos($request->header('Authorization'), $bearer) === false){
			return response()->json('Not Authorizated', 401);
		}

        return $next($request);
    }
}
